using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class ControladorCamara : MonoBehaviour
{
    public GameObject jugador;
    public Camera camara;
    public GameObject[] BloquePrefab;
    public float PunteroJuego;
    public float LugarSeguroGeneracion = 12;
    public bool Perdiste;
    public Text TextoDelJuego;



    // Start is called before the first frame update
    void Start()
    {
        PunteroJuego = -7;
        Perdiste = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (jugador != null)
        {
            camara.transform.position = new Vector3(
                jugador.transform.position.x, 
                camara.transform.position.y,
                camara.transform.position.z);
            TextoDelJuego.text = "Puntaje: " + Mathf.Floor(jugador.transform.position.x);
        }
        else
        {
            if(!Perdiste)
            {
                Perdiste = true;
                TextoDelJuego.text += "\nSe Acabo!\nPresione R para volver a comenzar";

            }
            if (Perdiste)
            {
                if (Input.GetKeyDown("r"))
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
            }
        }

        while (jugador != null && PunteroJuego<jugador.transform.position.x+LugarSeguroGeneracion)
        {
            int indiceBloque = Random.Range(0, BloquePrefab.Length - 1);
            if(PunteroJuego < 0)
            {
                indiceBloque = 5;
            }
            GameObject ObjetoBloque = Instantiate(BloquePrefab[indiceBloque]);
            ObjetoBloque.transform.SetParent(this.transform);
            Bloque bloque = ObjetoBloque.GetComponent<Bloque>();
            ObjetoBloque.transform.position = new Vector2(
                PunteroJuego + bloque.tama�o / 2,
                0);
            PunteroJuego += bloque.tama�o;
        }



    }

}

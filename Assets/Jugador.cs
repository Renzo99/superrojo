using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador : MonoBehaviour{

    public int fuerzaSalto;
    public int velocidadMovimiento;
    bool enElSuelo = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space") && enElSuelo)
        {
            enElSuelo = false;
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, fuerzaSalto));
        }
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(velocidadMovimiento,
            this.GetComponent<Rigidbody2D>() .velocity.y            
            );
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        enElSuelo = true;
        if(collision.gameObject.tag == "Obstaculo")
        {
            GameObject.Destroy(this.gameObject);
        }
    }

   
}
